<?php
/*
 * Plugin Name: Pwh Pagelines Branding Section Plugin
 * Plugin URI: #
 * Description: Add more content to the branding section. See Settings: "Pwh Branding Section"
 * Version: 1.0
 * Author: Technology Warehouses of America Inc., &reg;
 * Author URI: http://technologywarehouses.com
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

function pwh_branding_1() {
    $content = get_option("pwh_branding_text");
	if($content != ""){
	echo "
	<style>
		.pwh-branding-1 {
			float: right;
			position: relative;
			top: -100px;
			z-index: 50;
		}
	</style>
	";
	echo "<div class='pwh-branding-1'>$content</div>";
	}
}

function pwh_branding_section_menu() {
	add_options_page(
		'Pwh Branding Section Settings', 
		'Pwh Branding Section', 
		'manage_options', 
		'pwhbranding_uid', 
		'pwh_branding_plugin_settings' 
	);
}

function pwh_branding_plugin_settings() {
    if (!current_user_can('manage_options'))
      wp_die( __('You do not have sufficient permissions to access this page.') );
    $hidden_field_name = 'pwh_branding_hidden';
    $data_field_name1 = "pwh_branding_text";
    $opt_val1 = get_option($data_field_name1);
    if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
        $opt_val1 = $_POST[ $data_field_name1 ];
		update_option($data_field_name1, $opt_val1);
		?>
		<div class="updated"><p><strong>
		<?php _e('settings saved.', 'menu-test' ); ?>
		</strong></p></div>
		<?php
    }
    ?>
	<div class='wrap'>
	<h2>Pwh Branding Plugin Settings</h2>
	<form name="form1" method="post" action="">
		<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
		<p><?php _e("Html/Text Content:", 'menu-test' ); ?> 
		<br/>
		<i>This html/text will be inserted into the Branding Section (PageLines Framework)
		before the 'icons' div.</i><br/>
		<textarea type="text" rows=5 cols=40
			name="<?php echo $data_field_name1; ?>" ><?php echo $opt_val1; ?></textarea>
		</p>
		<hr />
		<p class="submit"><input type="submit" name="Submit" 
			class="button-primary" 
				value="<?php esc_attr_e('Save Changes') ?>" />
		</p>
	</form>
	</div>
<?php
}


add_action( 'admin_menu', 'pwh_branding_section_menu' );
add_action('pagelines_before_branding_icons','pwh_branding_1');
